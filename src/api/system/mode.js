import request from '@/utils/request'

// 查询模型列表
export function listMode(query) {
  return request({

    url: '/model/manage/modelLists',
    method: 'post',
    params: query
  })
}

// 新增模型
export function addMode(data) {
  console.info(data)
  return request({
    url: '/model/manage/add',
    method: 'post',
    data: data
  })
}
// 模型数据权限
export function dataScope(data) {
  return request({
    url: '/system/mode/dataScope',
    method: 'put',
    data: data
  })
}

// 删除模型
export function delMode(modeId) {
  return request({
    url: '/model/manage/remove/' + modeId,
    method: 'delete'
  })
}


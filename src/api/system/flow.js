import request from '@/utils/request'

// 查询部署工作了流
export function listPost(queryParams) {
  return request({
    url: '/flow/manage/getprocesslists',
    method: 'post',
    params: queryParams
  })
}

// 查询岗位详细
export function getPost(postId) {
  return request({
    url: '/system/post/' + postId,
    method: 'get'
  })
}

// 新增岗位
export function addPost(data) {
  return request({
    url: '/system/post',
    method: 'post',
    data: data
  })
}

// 修改岗位
export function updatePost(data) {
  return request({
    url: '/system/post',
    method: 'put',
    data: data
  })
}

// 删除一次部署的工作流
export function delPost(deploymentId) {
  return request({
    url: '/flow/manage/remove/' + deploymentId,
    method: 'delete'
  })
}

//上传一个工作流文件
export function uploadworkflow(formData, config) {
  return request({
    url: '/flow/manage/uploadworkflow',
    method: 'post',
	data: formData,
	headers: config
  })
}

//查询流程图
export function showresource(rowId) {
  return request({
    url: '/flow/manage/showresource?pdid=' + rowId,
    method: 'get',
  })
}

//挂起一个流程定义
export function suspendProcessDefinition(queryParams) {
  return request({
    url: '/flow/manage/suspendProcessDefinition?pdid=' + queryParams.pdid + '&flag=' + queryParams.cron + '&date=' + queryParams.date,
    method: 'get',
  })
}

//转为模型
export function exchangeProcessToModel(rowId) {
  return request({
    url: '/flow/manage/exchangeProcessToModel/' + rowId,
    method: 'get',
  })
}


//查看工作流定义
export function showProcessDefinition(pdid,resource) {
  return request({
    url: '/flow/manage/showProcessDefinition/' + pdid + '/' + resource,
    method: 'get',
  })
}
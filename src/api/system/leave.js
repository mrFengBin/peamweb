import request from '@/utils/request'
import { parseStrEmpty } from "@/utils/ruoyi";

// 查询请假列表
export function listLeave(query) {
  return request({
    url: '/leaveapply/list/',
    method: 'post',
    params: query
  })
}

// 查询请假类型
export function getLeave(leaveType) {
  return request({
    url: '/leaveapply/list/' + parseStrEmpty(leaveType),
    method: 'post'
  })
}

// 新增用户
export function addLeave(data) {
  return request({
    url: '/leaveapply/add',
    method: 'post',
    data: data
  })
}
// 删除用户
export function delLeave(userId) {
  return request({
    url: '/system/user/' + userId,
    method: 'post'
  })
}


